"""
Common debexpo variable to be used in any files that requires that information
"""

PROJECT = 'debexpo'
VERSION = '3.0'
AUTHOR = 'Debexpo maintainers (see AUTHORS)'
COPYRIGHT = '2008-2020, {}'.format(AUTHOR)
